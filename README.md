# GitLab CI template for Sphinx

This project implements a GitLab CI/CD template to create intelligent and beautiful documentation with [Sphinx](https://www.sphinx-doc.org).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/sphinx/gitlab-ci-sphinx@1.1.1
    # 2: set/override component inputs
    inputs:
      build-dir: website # ⚠ this is only an example
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/sphinx'
    ref: '1.1.1'
    file: '/templates/gitlab-ci-sphinx.yml'

variables:
  # 2: set/override template variables
  SPHINX_BUILD_DIR: website # ⚠ this is only an example
```

## Global configuration

The Sphinx template uses some global configuration used throughout all jobs.

| Input / Variable | Description                            | Default value     |
| ------------------------ | -------------------------------------- | ----------------- |
| `image` / `SPHINX_IMAGE` | The Docker image used to run Sphinx    | `ghcr.io/sphinx-doc/sphinx:latest` |
| `project-dir` / `SPHINX_PROJECT_DIR` | Sphinx project root directory | `.` |
| `source-dir` / `SPHINX_SOURCE_DIR` | Sphinx source directory (relative to `$SPHINX_PROJECT_DIR`) containing the Sphinx `conf.py` file | `source` |
| `build-dir` / `SPHINX_BUILD_DIR` | Sphinx build directory (relative to `$SPHINX_PROJECT_DIR`), the path will be declared as artifact | `build` |
| `requirements-file` / `SPHINX_REQUIREMENTS_FILE` | Requirements file. If the file is not found in the repository, requirements are read from the `SPHINX_REQUIREMENTS` variable | `requirements.txt` |
| `requirements` / `SPHINX_REQUIREMENTS` | Space separated requirements (ignored if a requirement file is found) | _none_ |
| `prebuild-script` / `SPHINX_PREBUILD_SCRIPT` | Pre-build hook script | `sphinx-pre-build.sh` |
| `PIP_INDEX_URL` | Python repository url | _none_ |
| `pip-opts` / `PIP_OPTS` | pip extra [options](https://pip.pypa.io/en/stable/cli/pip/#general-options) | _none_ |

## Jobs

### `sphinx-build` job

This job performs Sphinx **build**. It uses the following variable:

| Input / Variable | Description                                       | Default value     |
| ----------------------- | ------------------------------------------------- | ----------------- |
| `build-args` / `SPHINX_BUILD_ARGS` | [`sphinx-build` options](https://www.sphinx-doc.org/en/master/man/sphinx-build.html)) to be used in the build job, | `-M html` |


### `sphinx-lychee` job

This job checks links with [lychee](https://github.com/lycheeverse/lychee). It uses the following variable:

| Input / Variable | Description                                       | Default value     |
| ----------------------- | ------------------------------------------------- | ----------------- |
| `lychee-enabled` / `SPHINX_LYCHEE_ENABLED` | Set to `true` to enable this job                  | _none_ (disabled) |
| `lychee-image` / `SPHINX_LYCHEE_IMAGE` | The Docker image used to run [lychee](https://github.com/lycheeverse/lychee) | `registry.hub.docker.com/lycheeverse/lychee:latest` |
| `lychee-args` / `SPHINX_LYCHEE_ARGS` | [lychee arguments](https://github.com/lycheeverse/lychee#commandline-parameters) to execute | `--exclude-loopback $SPHINX_SOURCE_DIR/**/*.rst` |

## Publishing

:warning: this template is not a deployment template and it only builds a Sphinx project.

You might deploy the generated site using a [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/) job (there is [a variant for that](#gitlab-pages-variant)) or any other method you see fit.

## Variants

### GitLab Pages variant

Basically it copies the content of the sphinx generated site folder (`site` by default) to the `public` folder which is published by [GitLab pages](https://docs.gitlab.com/ee/user/project/pages/#how-it-works).

If you wish to use it, add the following to your `gitlab-ci.yml`:

```yaml
include:
  # main template
  - component: gitlab.com/to-be-continuous/sphinx/gitlab-ci-sphinx@1.1.1
  # GitLab pages variant
  - component: gitlab.com/to-be-continuous/sphinx/gitlab-ci-sphinx-pages@1.1.1
```
