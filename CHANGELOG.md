## [1.1.1](https://gitlab.com/to-be-continuous/sphinx/compare/1.1.0...1.1.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([93eb24c](https://gitlab.com/to-be-continuous/sphinx/commit/93eb24c74e568042231f3bc97a18b6253ed42ba5))

# [1.1.0](https://gitlab.com/to-be-continuous/sphinx/compare/1.0.0...1.1.0) (2024-1-27)


### Features

* migrate to CI/CD component ([44318df](https://gitlab.com/to-be-continuous/sphinx/commit/44318dff8c50c5e46f199e6b4d007496fafd8945))

# 1.0.0 (2023-12-18)


### Features

* initial template version ([3049a2a](https://gitlab.com/to-be-continuous/sphinx/commit/3049a2a7695b429859735580eb5be555593d2164))
