# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
spec:
  inputs:
    image:
      description: The Docker image used to run Sphinx
      default: ghcr.io/sphinx-doc/sphinx:latest
    build-args:
      description: '[`sphinx-build` options](https://www.sphinx-doc.org/en/master/man/sphinx-build.html)) to be used'
      default: -M html
    project-dir:
      description: Sphinx project root directory
      default: .
    source-dir:
      description: Sphinx source directory (relative to `$SPHINX_PROJECT_DIR`) containing the Sphinx `conf.py` file
      default: source
    build-dir:
      description: Sphinx build output directory (relative to `$SPHINX_PROJECT_DIR`)
      default: build
    requirements-file:
      description: Requirements file (relative to `$SPHINX_PROJECT_DIR`). If the file is not found in the repository, requirements are read from the `SPHINX_REQUIREMENTS` variable
      default: requirements.txt
    requirements:
      description: Space separated requirements (ignored if a requirement file is found)
      default: ''
    prebuild-script:
      description: Pre-build hook script (relative to `$SPHINX_PROJECT_DIR`)
      default: sphinx-pre-build.sh
    pip-opts:
      description: pip extra [options](https://pip.pypa.io/en/stable/cli/pip/#general-options)
      default: ''
    lychee-enabled:
      description: Enable lychee
      type: boolean
      default: false
    lychee-image:
      description: The Docker image used to run [lychee](https://github.com/lycheeverse/lychee)
      default: registry.hub.docker.com/lycheeverse/lychee:latest
    lychee-args:
      description: '[lychee arguments](https://github.com/lycheeverse/lychee#commandline-parameters) to execute'
      default: --exclude-loopback $SPHINX_SOURCE_DIR/**/*.rst
---
workflow:
  rules:
    # prevent MR pipeline originating from production or integration branch(es)
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $PROD_REF || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $INTEG_REF'
      when: never
    # on non-prod, non-integration branches: prefer MR pipeline over branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*tag(,[^],]*)*\]/" && $CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*branch(,[^],]*)*\]/" && $CI_COMMIT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*mr(,[^],]*)*\]/" && $CI_MERGE_REQUEST_ID'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*default(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*prod(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*integ(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*dev(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

variables:
  # variabilized tracking image
  TBC_TRACKING_IMAGE: registry.gitlab.com/to-be-continuous/tools/tracking:master

  SPHINX_PROJECT_DIR: $[[ inputs.project-dir ]]
  SPHINX_IMAGE: $[[ inputs.image ]]
  SPHINX_REQUIREMENTS: $[[ inputs.requirements ]]
  SPHINX_REQUIREMENTS_FILE: $[[ inputs.requirements-file ]]
  SPHINX_SOURCE_DIR: $[[ inputs.source-dir ]]
  SPHINX_BUILD_DIR: $[[ inputs.build-dir ]]
  SPHINX_PREBUILD_SCRIPT: $[[ inputs.prebuild-script ]]
  SPHINX_BUILD_ARGS: $[[ inputs.build-args ]]
  SPHINX_LYCHEE_ENABLED: $[[ inputs.lychee-enabled ]]
  SPHINX_LYCHEE_IMAGE: $[[ inputs.lychee-image ]]
  SPHINX_LYCHEE_ARGS: $[[ inputs.lychee-args ]]
  PIP_OPTS: $[[ inputs.pip-opts ]]

  # default production ref name (pattern)
  PROD_REF: /^(master|main)$/
  # default integration ref name (pattern)
  INTEG_REF: /^develop$/

# ==================================================
# Stages definition
# ==================================================
stages:
  - build
  - test
  - package-build
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production

.sphinx-scripts: &sphinx-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # List of typical bundles
    bundles="/etc/ssl/certs/ca-certificates.crt"                            # Debian/Ubuntu/Gentoo etc.
    bundles="${bundles} /etc/ssl/cert.pem"                                  # Alpine Linux
    bundles="${bundles} /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem"  # CentOS/RHEL 7
    bundles="${bundles} /etc/pki/tls/certs/ca-bundle.crt"                   # Fedora/RHEL 6
    bundles="${bundles} /etc/ssl/ca-bundle.pem"                             # OpenSUSE
    bundles="${bundles} /etc/pki/tls/cacert.pem"                            # OpenELEC

    # Try to find the right bundle to update it with custom CA certificates
    for bundle in ${bundles}
    do
      # import if bundle exists
      if [[ -f "${bundle}" ]]
      then
        # Import certificates in bundle
        echo "${certs}" | tr -d '\r' >> "${bundle}"

        log_info "Custom CA certificates imported in \\e[33;1m${bundle}\\e[0m"
        ca_imported=1
        break
      fi
    done

    if [[ -z "$ca_imported" ]]
    then
      log_warn "Could not import custom CA certificates !"
    else
      # $REQUESTS_CA_BUNDLE required by Python Requests, used by PIP
      export REQUESTS_CA_BUNDLE="${bundle}"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  function prepare_sphinx() {
    if [[ -f "${SPHINX_REQUIREMENTS_FILE}" ]]; then
      log_info "installing requirements from file ${SPHINX_REQUIREMENTS_FILE}"
      # shellcheck disable=SC2086
      pip install -r "${SPHINX_REQUIREMENTS_FILE}" ${PIP_OPTS}
    elif [[ "${SPHINX_REQUIREMENTS}" ]]; then
      log_info "installing requirements from variable 'SPHINX_REQUIREMENTS': ${SPHINX_REQUIREMENTS}"
      # shellcheck disable=SC2086
      pip install ${SPHINX_REQUIREMENTS} ${PIP_OPTS}
    else
      log_info "no requirements defined"
    fi

    if [[ -f "${SPHINX_PREBUILD_SCRIPT}" ]]; then
      log_info "--- \\e[32mpre-build hook\\e[0m (\\e[33;1m${SPHINX_PREBUILD_SCRIPT}\\e[0m) found: execute"
      chmod +x "${SPHINX_PREBUILD_SCRIPT}"
      "./${SPHINX_PREBUILD_SCRIPT}"
    fi
  }

  unscope_variables

  # ENDSCRIPT

.sphinx-base:
  image: $SPHINX_IMAGE
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "sphinx", "1.1.1"]
  before_script:
    - !reference [.sphinx-scripts]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd "${SPHINX_PROJECT_DIR}"
  variables:
    PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  # Cache downloaded dependencies and plugins between builds.
  # To keep cache across branches add 'key: "$CI_JOB_NAME"'
  # TODO (if necessary): define cache policy here
  cache:
    # cache shall be per branch per template
    key: "$CI_COMMIT_REF_SLUG-sphinx"
    paths:
      - ${PIP_CACHE_DIR}

sphinx-build:
  extends: .sphinx-base
  stage: build
  before_script:
    - !reference [.sphinx-scripts]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - cd "${SPHINX_PROJECT_DIR}"
    - prepare_sphinx
  script:
    - sphinx-build ${TRACE+-v} ${SPHINX_BUILD_ARGS} ${SPHINX_SOURCE_DIR} ${SPHINX_BUILD_DIR}
  artifacts:
    name: "sphinx build from $CI_COMMIT_REF_SLUG"
    paths:
      - ${SPHINX_PROJECT_DIR}/${SPHINX_BUILD_DIR}
    expire_in: 1 day

sphinx-lychee:
  extends: .sphinx-base
  image:
    name: "$SPHINX_LYCHEE_IMAGE"
    entrypoint: [""]
  stage: test
  script:
    - lychee ${TRACE+--verbose} ${SPHINX_LYCHEE_ARGS}
  rules:
    - if: '$SPHINX_LYCHEE_ENABLED != "true"'
      when: never
    - !reference [.test-policy, rules]
